﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace Tunynet.Common
{
    /// <summary>
    /// 后台身份验证
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class ConsoleAuthorizeAttribute : FilterAttribute, IAuthorizationFilter
    {
        #region IAuthorizationFilter 成员

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
            {
                IAuthenticationService authenticationService = DIContainer.ResolvePerHttpRequest<FormsAuthenticationService>();
                authenticationService.SignOut();
                filterContext.Result = new RedirectResult(SiteUrls.Instance().Login(HttpUtility.UrlEncode(filterContext.HttpContext.Request.RawUrl)));
            }

            Authorizer authorizer = DIContainer.Resolve<Authorizer>();
             IUser user = UserContext.CurrentUser;
            if (user == null)
                filterContext.Result = new RedirectResult(SiteUrls.Instance().Login(HttpUtility.UrlEncode(filterContext.HttpContext.Request.RawUrl)));
            else if (!authorizer.AuthorizeCore(user))
            {
                Dictionary<string, string> buttonLink = new Dictionary<string, string>();
                buttonLink.Add("首页", SiteUrls.Instance()._Perfecthref(SiteUrls.Instance().Home()));
                filterContext.Controller.TempData["SystemMessageViewModel"] = new SystemMessageViewModel
                {
                    Body = "您可能没有权限查看此页面,<br/><span id='seconds'>5</span>秒后，自动跳转到",
                    ReturnUrl = SiteUrls.Instance().Home(),
                    Title = "无权查看",
                    StatusMessageType = StatusMessageType.Error,
                    ButtonLink = buttonLink
                };

                filterContext.Result = new RedirectResult(SiteUrls.Instance().SystemMessage());
            }
        }

        #endregion IAuthorizationFilter 成员

        // This method must be thread-safe since it is called by the thread-safe OnCacheAuthorization() method.
    
    }
}