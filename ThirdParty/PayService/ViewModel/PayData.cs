﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

namespace Tunynet.PayServer
{
    /// <summary>
    /// 支付统一编辑实体
    /// </summary>
    public class PayData
    {
        /// <summary>
        /// 订单内容
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// 商户网站唯一订单号
        /// </summary>
        public string OutTradeNo { get; set; }

        /// <summary>
        /// 商品的标题/交易标题/订单标题/订单关键字等。
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// 订单总金额，单位为元，精确到小数点后两位，取值范围[0.01,100000000]
        /// </summary>
        public string TotalAmount { get; set; }

        /// <summary>
        ///回调Url
        /// </summary>
        public string ReturnUrl { get; set; }

        /// <summary>
        ///通知Url
        /// </summary>
        public string NotifyUrl { get; set; }
    }
}