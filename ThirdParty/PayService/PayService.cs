﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using Aop.Api;
using Aop.Api.Domain;
using Aop.Api.Request;
using Aop.Api.Response;
using Jayrock.Json.Conversion;
using Senparc.Weixin.MP.TenPayLibV3;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace Tunynet.PayServer
{
    /// <summary>
    /// 支付统一业务逻辑
    /// </summary>
    public class PayService
    {
        private IAopClient client;

        public PayService(IAopClient client)
        {
            this.client = client;
        }

        #region 阿里云支付

        /// <summary>
        /// 创建阿里云支付请求表单
        /// </summary>
        /// <param name="PayData">支付实体</param>
        /// <returns>返回支付宝表单FormBodyHTML</returns>
        public string AliPayCreateOrder(PayData payData)
        {
            AlipayTradeWapPayModel atwpEntity = new AlipayTradeWapPayModel();
            ////打开页面
            atwpEntity.OutTradeNo = payData.OutTradeNo;
            atwpEntity.ProductCode = "FAST_INSTANT_TRADE_PAY";
            atwpEntity.TotalAmount = payData.TotalAmount;
            atwpEntity.Subject = payData.Subject;
            atwpEntity.Body = payData.Body;
            atwpEntity.GoodsType = "1";
            var request = new AlipayTradePagePayRequest();
            request.SetReturnUrl(payData.ReturnUrl);
            request.SetNotifyUrl(payData.NotifyUrl);
            request.SetBizModel(atwpEntity);
            AlipayTradePagePayResponse response = client.pageExecute(request);
            response.Body = response.Body.Replace("<form ", "<form   style='display:none;' ");
            return response.Body;
        }

        /// <summary>
        /// 查询Ali云订单是否成功
        /// </summary>
        /// <param name="orderId">订单Id</param>
        /// <returns>
        /// 返回参数对应的KV值
        ///"code": "10000",
        ///"msg": "Success",
        ///"trade_no": "2013112011001004330000121536",
        ///"out_trade_no": "6823789339978248",
        ///"open_id": "2088102122524333",
        ///"buyer_logon_id": "159****5620",
        ///"trade_status": "TRADE_CLOSED",
        ///"total_amount": 88.88,
        ///"receipt_amount": "15.25",
        ///"buyer_pay_amount": 8.88,
        ///"point_amount": 10,
        ///"invoice_amount": 12.11,
        ///"send_pay_date": "2014-11-27 15:45:57",
        ///"alipay_store_id": "2015040900077001000100001232",
        ///"store_id": "NJ_S_001",
        ///</returns>
        public IDictionary AliPayTradeQuery(long orderId)
        {
            var request = new AlipayTradeQueryRequest();
            AlipayTradeQueryModel alipayTradeQueryModel = new AlipayTradeQueryModel();
            alipayTradeQueryModel.OutTradeNo = orderId.ToString();
            request.SetBizModel(alipayTradeQueryModel);
            AlipayTradeQueryResponse response = client.Execute(request);
            string form = response.Body;
            IDictionary json = JsonConvert.Import(response.Body) as IDictionary;
            var data = JsonConvert.Import(json["alipay_trade_query_response"].ToString()) as IDictionary;
            return data;
        }

        #endregion 阿里云支付

        #region 微信支付

        /// <summary>
        /// 生成随机串，随机串包含字母或数字
        /// </summary>
        /// <returns></returns>
        public static string GenerateNonceStr()
        {
            return Guid.NewGuid().ToString().Replace("-", "");
        }

        /// <summary>
        /// 微信创建支付(PC二维码2)
        /// </summary>
        /// <param name="payData">支付实体</param>
        /// <returns></returns>
        public string WxPayCreateOrder(PayData payData)
        {
            TenPayV3UnifiedorderRequestData requestData = new TenPayV3UnifiedorderRequestData(PayConfig.WxAPPID, PayConfig.WxMCHID, payData.Body, payData.OutTradeNo, Convert.ToInt32(payData.TotalAmount), "8.8.8.8", payData.NotifyUrl, Senparc.Weixin.MP.TenPayV3Type.NATIVE, null, PayConfig.WxKEY, GenerateNonceStr());
            var unifiedorderResult = TenPayV3.Unifiedorder(requestData);
            return unifiedorderResult.code_url;
        }

        /// <summary>
        /// 微信获取回调信息
        /// </summary>
        /// <returns>
        /// out_trade_no:订单号,
        /// transaction_id:交易流水号,
        /// openid:用户在appid下的 唯一标识
        /// </returns>
        public IDictionary WxGetNotifyData(HttpRequestBase Request)
        {
            //接收从微信后台POST过来的数据
            System.IO.Stream s = Request.InputStream;
            int count = 0;
            byte[] buffer = new byte[1024];
            StringBuilder builder = new StringBuilder();
            while ((count = s.Read(buffer, 0, 1024)) > 0)
            {
                builder.Append(Encoding.UTF8.GetString(buffer, 0, count));
            }
            s.Flush();
            s.Close();
            s.Dispose();
            Dictionary<string, string> data = new Dictionary<string, string>();
            try
            {
                OrderQueryResult orderQueryResult = new OrderQueryResult(builder.ToString());
                //微信订单号
                data.Add("transaction_id", orderQueryResult.transaction_id);
                //自己生产的订单号
                data.Add("out_trade_no", orderQueryResult.out_trade_no);
                //微信支付终端号
                data.Add("openid", orderQueryResult.openid);
                return data as IDictionary;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion 微信支付
    }
}