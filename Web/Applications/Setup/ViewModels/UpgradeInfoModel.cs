﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Spacebuilder.Setup
{
    /// <summary>
    /// 升级信息实体
    /// </summary>
    public class UpgradeInfoModel
    {
        /// <summary>
        /// 当前升级版本
        /// </summary>
        public string CurrentVersion { get; set; }

        /// <summary>
        /// 升级模块
        /// </summary>
        public List<string> UpgradeModule { get; set; }

        /// <summary>
        /// 旧数据库连接串
        /// </summary>
        [Required(ErrorMessage = "请填写旧数据库连接串")]
        [AllowHtml]
        public string OldConnectionStrings { get; set; }

        /// <summary>
        /// 旧版附件目录路径
        /// </summary>
        [Required(ErrorMessage = "请填写旧版附件目录路径")]
        [AllowHtml]
        public string OldFilePath { get; set; }
    }
}