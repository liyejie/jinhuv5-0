﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using PetaPoco;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Tunynet;
using Tunynet.Settings;
using Tunynet.Utilities;

namespace Spacebuilder.Setup
{
    public class SetupHelper
    {
        /// <summary>
        /// 执行sql脚本
        /// </summary>
        /// <param name="db"></param>
        /// <param name="pathToScriptFile"></param>
        /// <param name="messages"></param>
        /// <param name="adminInfo"></param>
        /// <param name="mainSiteUrl"></param>
        public static void ExecuteInFile(Database db, string pathToScriptFile, out ConcurrentDictionary<string, string> messages, Dictionary<string, string> adminInfo = null, string mainSiteUrl = "")
        {
            string sqlString = "";
            StreamReader reader = null;
            messages = new ConcurrentDictionary<string, string>();

            if (!System.IO.File.Exists(pathToScriptFile))
            {
                throw new Exception("文件" + pathToScriptFile + " 未找到!");
            }
            Stream stream = System.IO.File.OpenRead(pathToScriptFile);
            reader = new StreamReader(stream);
            try
            {
                db.OpenSharedConnection();
            }
            catch (Exception e)
            {
                messages[e.Message] = e.StackTrace;
                reader.Close();
                return;
            }
            db.CommandTimeout = 600;
            while (null != (sqlString = ReadNextFromStream(reader, adminInfo, mainSiteUrl)))
            {
                try
                {
                    db.Execute(sqlString);
                }
                catch (Exception e)
                {
                    if (!messages.ContainsKey("在文件：" + pathToScriptFile + " 中产生异常"))
                        messages["在文件：" + pathToScriptFile + " 中产生异常"] = e.Message;
                    else
                        messages["在文件：" + pathToScriptFile + " 中产生异常"] += e.Message;
                    if (!messages.ContainsKey(e.Message))
                        messages[e.Message] = e.StackTrace;
                    else
                        messages[e.Message] += e.StackTrace;
                    reader.Close();
                    return;
                }
            }
            reader.Close();
            db.CloseSharedConnection();
        }

        /// <summary>
        /// 读取文件中的下一行
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="adminInfo"></param>
        /// <param name="siteName"></param>
        /// <returns></returns>
        public static string ReadNextFromStream(StreamReader reader, Dictionary<string, string> adminInfo, string siteName)
        {
            string textLine;
            StringBuilder stringBuilder = new StringBuilder();
            if (!string.IsNullOrEmpty(siteName))
            {
                var settings = DIContainer.Resolve<ISettingsManager<SiteSettings>>();
                var setting = settings.Get();
                setting.SiteName = siteName;
                settings.Save(setting);
            }
            while (true)
            {
                textLine = reader.ReadLine();
                if (textLine != null)
                {
                    foreach (var item in adminInfo)
                    {
                        if (textLine.Contains(item.Key))
                        {
                            textLine = textLine.Replace(item.Key, item.Value);
                        }
                    }
                }

                if (textLine == null)
                {
                    if (stringBuilder.Length > 0)
                    {
                        return stringBuilder.ToString();
                    }
                    else
                    {
                        return null;
                    }
                }
                if (textLine.TrimEnd().ToUpper() == "GO")
                {
                    continue;
                }
                stringBuilder.AppendFormat("{0}\r\n", textLine);
            }
            return stringBuilder.ToString();
        }

        /// <summary>
        /// 获取安装脚本
        /// </summary>
        /// <returns></returns>
        public static List<string> GetInstallFiles(DBType dbType, IList<string> applicationKeys = null, bool createAdministrator = false)
        {
            List<string> fileList = new List<string>();

            if (applicationKeys == null)
            {
                applicationKeys = new List<string>() { "Common" };
                var applicationsDirectory = WebUtility.GetPhysicalFilePath("~/Applications/");
                foreach (string appPath in Directory.GetDirectories(applicationsDirectory))
                {
                    DirectoryInfo directoryInfo = new DirectoryInfo(appPath);
                    if (directoryInfo != null & directoryInfo.Name != "Setup")
                        applicationKeys.Add(directoryInfo.Name);
                }
            }
            string filePath;
            if (applicationKeys.Contains("Common"))
            {
                filePath = WebUtility.GetPhysicalFilePath("~/Applications/Setup/Setup/Install/" + dbType.ToString() + "/");

                if (Directory.Exists(filePath))
                    fileList.AddRange(Directory.GetFiles(filePath).ToList());
            }
            foreach (var applicationKey in applicationKeys)
            {
                filePath = WebUtility.GetPhysicalFilePath("~/Applications/" + applicationKey + "/Setup/Install/" + dbType.ToString() + "/");
                if (Directory.Exists(filePath))
                    fileList.AddRange(Directory.GetFiles(filePath).ToList());
            }

            string temp = fileList.FirstOrDefault(n => n.Contains("CreateAdministrator"));
            fileList.Remove(temp);
            if (createAdministrator)
            {
                fileList.Add(temp);
            }
            return fileList;
        }

        ///// <summary>
        ///// 获取重命名旧表脚本
        ///// </summary>
        ///// <returns></returns>
        //public static string GetUpgradeReNameFile()
        //{
        //    string filePath = WebUtility.GetPhysicalFilePath("~/Applications/Setup/Setup/Upgrade/SqlServer/01_Upgrade_ReName_v3.2_to_v4.1.sql");
        //    if (File.Exists(filePath))
        //        return filePath;
        //    return string.Empty;
        //}

        /// <summary>
        /// 获取升级脚本
        /// </summary>
        /// <returns></returns>
        public static List<string> GetUpgradeFiles(DBType dbTyp)
        {
            string filePath = WebUtility.GetPhysicalFilePath($"~/Applications/Setup/Setup/Upgrade/{dbTyp.ToString()}/");
            if (Directory.Exists(filePath))
                return Directory.GetFiles(filePath).ToList();
            return new List<string>();
        }
    }
}