﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using System.Web.Routing;
using Tunynet.Common;

namespace Tunynet.Spacebuilder
{ 
    /// <summary>
    /// 资讯标签云Url获取
    /// </summary>
public class ContentItemTagUrlGetter : ITagUrlGetter
    {
        /// <summary>
        /// 获取链接
        /// </summary>
        /// <param name="tagName"></param>
        /// <returns></returns>
        public string GetUrl(long tagId, long ownerId = 0)
        {
            RouteValueDictionary routeValueDictionary = new RouteValueDictionary();
            routeValueDictionary.Add("tagId", tagId);
            return CachedUrlHelper.Action("TagCMS", "CMS", null, routeValueDictionary);
        }
    }
}