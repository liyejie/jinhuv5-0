﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using Tunynet.Common;

namespace Tunynet.Spacebuilder
{
    /// <summary>
    /// 评论被举报url详情获取
    /// </summary>
    public class CommentImpeachUrlGetter : IImpeachUrlGetter
    {
        public CommentService commentService;

        public CommentImpeachUrlGetter(CommentService commentService)
        {
            this.commentService = commentService;
        }

        /// <summary>
        /// 租户类型Id
        /// </summary>
        public string TenantTypeId
        {
            get { return TenantTypeIds.Instance().Comment(); }
        }

        /// <summary>
        /// 获取被举报的对象实体
        /// </summary>
        /// <param name="reportObjectId">被举报相关对象Id</param>
        /// <returns></returns>
        public ImpeachObject GetImpeachObject(long reportObjectId)
        {
            var comment = commentService.Get(reportObjectId);

            if (comment != null)
            {
                var commentUrlGetter = CommentUrlGetterFactory.Get(comment.TenantTypeId);
                var commentedObject = commentUrlGetter.GetCommentedObject(comment.CommentedObjectId, 0);
                ImpeachObject impeachObject = new ImpeachObject()
                {
                    DetailUrl = commentedObject.DetailUrl.Replace("comid-0", "comid-" + reportObjectId),
                    Name = commentedObject.Name,
                    UserId = commentedObject.UserId
                };
                return impeachObject;
            }
            return new ImpeachObject();
        }
    }
}